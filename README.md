# README #

Jekyll tema prebačena iz statičkog HTML predloška za potrebe izrade stranice Skaville festivala. Tema se prilikom svakog git commita deploya na server korištenjem [Deploybota](https://deploybot.com/)

* Verzija 1.0
* [Upute za korištenje Jekylla i teme](https://jekyllrb.com/docs/usage/)

### Izrada ###

* Viktor Prevaric